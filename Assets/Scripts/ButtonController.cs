﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonController : MonoBehaviour {

	private Button button;

	// Disable the buttons at the start of the game
	void Start () {
		button = GetComponent<Button>();
		button.interactable = false;
	}
	
	// Make the button interactable if the game is not paused.
	void Update () {
		button.interactable = MenuController.isPaused();
	}
}

﻿using UnityEngine;
using System.Collections;

public class TorchBehavior : MonoBehaviour {

	/*
	 * The total number of torches the player has collected throughout the game
	 */
	public static int torchCount;

	/*
	 * The total number of torches the player has collected on this level
	 */
	public static int torchCountOnLevel;

	/*
	 * The TorchMania animator (default torch animator)
	 */
	public Animator anime;

	/*
	 * Whether or not this torch has been extinguished
	 */
	public bool torchDone;

	/*
	 * The number of torches on the loaded level
	 */
	public static int torchesOnLevel;

	void Start(){
		anime.SetBool("Torch" , false);
		torchesOnLevel = GameObject.FindGameObjectsWithTag("Torch").Length;
		torchCountOnLevel = 0;
	}

	void OnTriggerEnter2D(Collider2D other) {
		if(other.CompareTag("Player") && torchDone == false)
		{
			anime.SetBool("Torch" , true);
			torchDone = true;

			// Disable tourch lights after extinguishing
			foreach(Transform child in transform){
				Light light;
				if(child.CompareTag("Light")){
					light = child.GetComponent<Light>();

					if(child.name == "Source Light"){
						light.intensity = 0.5F;
					} else if (child.name == "Background Light"){
						light.intensity = 1F;
					} else if (child.CompareTag("Light")){
						light.enabled = false;
					}
				}
			}

			GetComponent<AudioSource>().Play ();

			torchCountOnLevel++;
			torchCount++;
		}
	}

	public static bool accessGranted (){
		return torchCountOnLevel >= torchesOnLevel;
	}
}

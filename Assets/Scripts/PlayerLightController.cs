﻿using UnityEngine;
using System.Collections;

public class PlayerLightController : MonoBehaviour {

	public Transform player;

	// Use this for initialization
	void Start () {
		if(!player){
			player = GameObject.FindWithTag ("Player").transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3(player.position.x, player.position.y, transform.position.z);
	}
}

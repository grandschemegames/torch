﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
	private Canvas canvas;
	private static bool paused;

	void Start(){
		canvas = GetComponent<Canvas>();
		if(canvas){
			canvas.enabled = false;
		}
	}

	void Update(){
		if(Input.GetButtonDown("Cancel")){
			OnPause();
		}
	}

	/*
	 * Start the game
	 */
	public void OnStart() {
		Application.LoadLevel("Level 1");
	}


	/*
	 * Restart the game
	 */
	public void OnRestart() {
		OnStart ();
	}

	/*
	 * Quit the game
	 */
	public void OnQuit(){
		Application.Quit();
	}

	/*
	 * Whether or not the game is paused
	 */
	public static bool isPaused(){
		return paused;
	}

	/*
	 * Pause the game
	 */
	public void OnPause(){
		if(!canvas){
			return;
		}

		paused = !paused;

		if(Time.timeScale != 0){
			Time.timeScale = 0;
			canvas.enabled = true;
		} else {
			Time.timeScale = 1;
			canvas.enabled = false;
		}
	}
}

﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class DoorController : MonoBehaviour {

	/*
	 * The next level the player will reach after completing the current level. This value
	 * starts on the current level and is incremented when the door opens for the next level.
	 */
	public static int nextLevel = 1;

	/*
	 * The door animator
	 */
	public Animator anime;

	private string levelName;

	// This function runs when a new scene is loaded
	void Start () {
		levelName = Application.loadedLevelName;
		string[] levelNameList = levelName.Split( new char[] {' '});
		nextLevel = Int32.Parse(levelNameList[levelNameList.Length - 1]);
	}
	
	// Update is called once per frame
	void Update () {
		// See if the door can be opened only if it is closed.
		if(!anime.GetBool ("Door Open")){
			doorOpen();
		}
	}

	/*
	 * Check to see if the door may be opened and the level may be advanced.
	 */
	void doorOpen ()
	{
		if(TorchBehavior.accessGranted())
		{
			anime.SetBool("Door Open", true);
//			Debug.Log("Congrats Breh! " + levelName + " complete!");
			GetComponent<AudioSource>().Play();
			nextLevel+= 1;
		}
	}

	/*
	 * Handler for when the player touches the door
	 */
	void OnTriggerEnter2D (Collider2D other) 
	{
//		Debug.Log ("Torch count: " + TorchBehavior.torchCount + " nextLevel: " + nextLevel);
		if(other.CompareTag("Player") && TorchBehavior.accessGranted()) 
		{
			if(nextLevel == 8){
				Application.LoadLevel ("The End");
			} else {
				Application.LoadLevel ("Level " + nextLevel);
			}
		}
	}
}

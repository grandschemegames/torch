﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

	/*
	 * The Player animator. The default one is the Animator for the Stick Bro_0 sprite.
	 */
	public Animator anime;

	/*
	 * The maximum speed for the player
	 */
	public float maxSpeed;

	/*
	 * The vertical jumping force
	 */
	public float jumpPower;

	/*
	 * Whether or not the player is jumping 
	 */
	public bool isJumping;

	/*
	 * A child component of Stick Bro that keeps track of the player's distance from the ground
	 */
	public Transform groundCheck;

	/*
	 * The distance from a "Ground" component the player must be in order to be considered jumping
	 */
	public float groundRadius;

	/*
	 * The Layer that a component needs in order to be considered ground
	 */
	public LayerMask whatIsGround;

	/*
	 */
	private Rigidbody2D rb;

	/*
	 * Called at the start of every scene
	 */
	void Start(){
		rb = GetComponent<Rigidbody2D>();
	}

	/*
	 * Called right before all physics rendering
	 */
	void FixedUpdate(){
		isJumping = Physics2D.OverlapCircle(groundCheck.position, groundRadius, whatIsGround);
		anime.SetBool("Grounded", true);
		
		Movement();
	}

	/*
	 * Called every frame
	 */ 
	void Update(){
		Jump ();
	}

	/*
	 * Control the player's movement
	 */
	void Movement(){
		if(Input.GetButton("Horizontal")){
			// Cache horizontal input
			float horizontal = Input.GetAxis("Horizontal");
			
			// Set speed equal to the absolute value of the horizontal input
			anime.SetFloat("Player Speed", Mathf.Abs(horizontal));
			
			// Moves the character to the right when the right arrow key is pressed
			if (horizontal > 0){
				transform.Translate(Vector2.right * maxSpeed * Time.deltaTime);
				transform.eulerAngles = new Vector2(0,0);
			}
			
			// Moves the character to the left while flipling its current "Y-axis" over by 180 degrees 
			if(horizontal < 0)
			{
				transform.Translate(Vector2.right * maxSpeed * Time.deltaTime);
				transform.eulerAngles = new Vector2(0,180);
			}
		}
	}

	void Jump () 
	{
		// Engine uses the player's circle collider to check to see if anything is touching it
		// Character may jump only if its circle collider is touching an object with layer named "Ground"
		if(isJumping && Input.GetButtonDown("Jump"))
		{
			anime.SetBool("Grounded",false);
			rb.AddForce(new Vector2 (0,jumpPower));
		}
		else
		{
			return;
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class MovingPlatform : MonoBehaviour {
	/*
	 * The platform's startin x position
	 */
	[SerializeField]
	private float xPosition;

	/*
	 * The platform's startin y position
	 */
	[SerializeField]
	private float yPosition;

	/*
	 * Whether or not the platform should reverse direction
	 */
	public bool max;

	/*
	 * Whether or not this platform moves vertically
	 */
	public bool vertical;

	/*
	 * Whether or not this platform moves horizontally
	 */
	public bool horizontal;

	/*
	 * Whether or not this platform will switch directions
	 */
	public bool switchDirection;

	/*
	 * Whether or not this platform has switched directions
	 */
	private bool switched;

	public int maxDistanceX;
	public int maxDistanceY;

	/*
	 * The amount of units the platform moves per frame
	 */
	public float step;

	// Use this for initialization
	void Start () {
		xPosition = transform.position.x;
		yPosition = transform.position.y;
	}
	
	// Update is called once per frame
	void Update () {
		if(vertical)//Vertical
		{ 
			if(transform.position.y >= yPosition + maxDistanceY)
			{
				max = true;
				switched = false;
			} else if(transform.position.y <= yPosition)
			{
				max = false;

				if(switchDirection && !switched && transform.position.y <= yPosition){
					vertical = false;
					horizontal = true;
					switched = true;
				}
			}
		}
		
		if(horizontal)//Horizontal
		{
			if(transform.position.x >= xPosition + maxDistanceX)
			{
				max = true;
				switched = false;
			} else if(transform.position.x <= xPosition)
			{
				max = false;

				if(switchDirection && !switched && transform.position.x <= xPosition){
					horizontal = false;
					vertical = true;
					switched = true;
				}
			}
		}

		float translation = Time.deltaTime * step;

		// MOVING THE PLATFORM
		if(vertical)// Vertical Mmvement
		{
			if(!max) 
			{
				transform.position = new Vector3(transform.position.x, transform.position.y + translation, transform.position.z);
			} else 
			{
				transform.position = new Vector3(transform.position.x, transform.position.y - translation, transform.position.z);
			}
		} 
		if(horizontal)//Horizontal Movement
		{ 
			if(!max)
			{
				transform.position = new Vector3(transform.position.x + translation, transform.position.y, transform.position.z);
			} else 
			{
				transform.position = new Vector3(transform.position.x - translation, transform.position.y, transform.position.z);
			}
		}
	}

	void OnCollisionEnter2D (Collision2D coll) 
	{
		if(coll.gameObject.name == "Stick Bro") 
		{
			coll.transform.parent = transform;
		} 
		else
		{
			coll.transform.parent = null;
		}
	}
	
	void OnCollisionExit2D (Collision2D coll) 
	{
		if(coll.gameObject.name == "Stick Bro") 
		{
			coll.transform.parent = null;
		}
	}
}

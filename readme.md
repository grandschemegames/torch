# Torch
This is the project for our first game, based on Unity. Please see our [development guide](https://grandschemegames.atlassian.net/wiki/display/PG/Development) for advice on contributing.

## Controls
* Z or Space to Jump
* Arrow keys or W,A,S,D to move
* Escape to pause